package com.example.mydictionary;

public interface ListItemListener {
    void onItemClick(int position);
}
