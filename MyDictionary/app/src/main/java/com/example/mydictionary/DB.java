package com.example.mydictionary;

public class DB {
    public static String[] getData(int id){
        if (id == R.id.action_vn_vn){
            return getVnVn();
        } else if (id == R.id.action_vn_ger){
            return getVnGer();
        } else if (id == R.id.action_ger_vn){
            return getGerVn();
        }
        return new String[0];
    }

    public static String[] getGerVn() {
        String[] source = new String[]{
                "warum",
                "wie",
                "wann",
                "wo",
                "woran",
                "worum",
                "welche",
                "welches",
        };
        return source;
    }

    public static String[] getVnGer() {
        String[] source = new String[]{
                "linh",
                "Linh",
                "lInh",
                "liNh",
                "linH",
                "LInh",
                "LINh",
                    "lINh",
        };
        return source;
    }

    public static String[] getVnVn() {
        String[] source = new String[]{
                "baka",
                "trai",
                "dep",
                "thuong",
                "de",
                "lam",
                "vi",
                "thao",
                "hoang",
                "vuong",
                "nguyen",
                "duc",
                "linh",
                "trang",
                "tRang",
                "trAng",
                "traNg",
                "tranG",
                "Trang",
                "TRang",
                "trANg",
                "TRAng",
        };
        return source;
    }
}
